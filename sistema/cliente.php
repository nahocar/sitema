<?php
    session_start();

    include "../conexion.php";

    if(!empty($_POST))
    {
        $alert= '';
        if(empty($_POST['cuit']) || empty($_POST['rsocial']) || empty($_POST['telefono']) || empty($_POST['direccion']))
        {
            $alert= '<p class="msg_error"> Todos los campos son obligatorios.</p>';
        }else{

            $cuit       = $_POST['cuit'];
            $rsocial    = $_POST['rsocial'];
            $telefono   = $_POST['telefono'];
            $direccion  = $_POST['direccion'];
            $usuario_id = $_SESSION['idUser'];

            $result = 0;

            if(is_numeric($cuit))
            {
                $query = mysqli_query($conection,"SELECT * FROM cliente WHERE cuit = '$cuit' ");
                $result = mysqli_fetch_array($query);
            }

            if($result > 0){
                $alert= '<p class="msg_error"> El numero de CUIT ya existe</p>';
            }else{
                $query_insert = mysqli_query($conection,"INSERT INTO cliente(cuit,rsocial,telefono,direccion,usuario_id)
                                                                     VALUES('$cuit','$rsocial','$telefono','$direccion','$usuario_id')");

                if($query_insert){
                    $alert= '<p class="msg_save"> Cliente guardado correctamente</p>';
                }else{
                    $alert= '<p class="msg_error"> Error al guardar el cliente</p>';
                }
            } 
        }
        mysqli_close($conection); 
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php include "scripts.php"; ?>
	<title>Registro Cliente</title>
</head>
<body>
	<?php include "header.php"; ?>
	<section id="container">
        
        <div class="form_register">
            <h1>Registro de cliente</h1>
            <hr>
            <div class="alert"><?php echo isset($alert) ? $alert : ''; ?></div>

                <form action="" method="post">
                    <label for="cuit">CUIT</label>
                    <input type="number" name="cuit" id="cuit" placeholder="N° CUIT">
                    <label for="rsocial">Razón Social</label>
                    <input type="text" name="rsocial" id="rsocial" placeholder="Razón Social">
                    <label for="telefono">Telefono</label> 
                    <input type="number" name="telefono" id="telefono" placeholder="Telefono">
                    <label for="direccion">Dirección</label>
                    <input type="text" name="direccion" id="direccion" placeholder="Dirección">
                    <input type="submit" value="Guardar Cliente" class="btn_save">
                </form>
            
        </div>
    </section>

	<?php include "footer.php"; ?>
</body>
</html>